# Generating Online/offline response 3D histograms (AURORA):

* Create the top directory for this project\
`mkdir onlineOverOfflineStudies`
* Move into that directory\
`cd onlineOverOfflineStudies`
* Clone the TLASteering repository\
`git clone https://gitlab.cern.ch/atlas-phys-exotics-dijet-tla/TLASteeringFullRun2.git`
* Move into that directory\
`cd TLASteeringFullRun2`
* Setup the necessary environment, this will not run to completion this time\
`source setup.sh`
* Go up one directory\
`cd ..`
* Run the install\
`source install.sh`
* Fix some residuals stuff\
`cd src/TLAAlgosFullRun2/`\
`git pull origin master`\
`cd ../../build`\
`cmake ../src`\
`make -j32`\
* Move into the project specific run directoy\
`cd ../run/onlineOverOffline/`
* Change the version name of the output files to something arbitrary like v22, save and exit vim\
`vim run_dataMjjOnlineOverOffline.sh`
* Submit using\
`sbatch run_dataMjjOnlineOverOffline.sh`
* Open the run file, comment out the current "datasetNumber", uncomment the one below, save and exit\
`vim run_dataMjjOnlineOverOffline.sh`
* submit again\
`sbatch run_dataMjjOnlineOverOffline.sh`
* Repeat until you have submitted for all 13 datasetNumbers. I know this is a dumb way to do it, but due to the way sbatch works, this is the only way possible.
* When the jobs are finished the results should all appear inside a automatically created directory at `/TLASteeringFullRun2/run/onlineOverOffline/VERSIONNUMBER`

## General information
* The config lives at `TLASteeringFullRun2/configs/onlineOverOffline/config_dataMjjOnlineOverOffline.py`
  * Here you can change what calibration, triggers, and cuts are used. Currently, the config has defined that we only run HLT_175 jets with an eta cut of 2.4
* The run file lives at `TLASteeringFullRun2/run/onlineOverOffline/run_dataMjjOnlineOverOffline.sh`
* The algo lives at `TLASteeringFullRun2/src/TLAAlgosFullRun2/Root/ResponseAlgo.cxx`
* The algo header file lives at `TLASteeringFullRun2/src/TLAAlgosFullRun2/TLAAlgosFullRun2/ResponseAlgo.h`

# Plotting Online/offline response plots (Locally):
* On your local computer, or where you can easily run Jupyter notebook, clone the repository
`git clone https://gitlab.cern.ch/pekman/responseAndResolutionStudies.git`
* Enter the newly cloned directory and it's data directory\
`cd responseAndResolutionStudies/data`
* Download your ResponseAlgo.cxx generated data into the data directory. For AURORA users, replace USERNAME in:\
`scp -r USERNAME@aurora.lunarc.lu.se:/home/USERNAME/onlineOverOfflineStudies/TLASteeringFullRun2/run/onlineOverOffline/VERSIONNUMBER ./`
* Enter the specific data directory. For example:\
`cd VERSIONNUMBER`
* Merge the .root files  using\
`hadd merged.root ./data16*/hist-inputFileList_data16*`
* Exit the data directory\
`cd ../..`
* Create the python environment using conda. This will take ~60 min due to the ROOT package\
`conda env create -f environment.yml`
* To solve a recent issue with PyROOT do\
`sudo apt-get install libpython3.7`
* Activate the newly created environment\
`conda activate responseStudies`
* Start Jupyter Notebook\
`jupyter notebook`
* Open `responseStudies.ipynb` and follow the instructions there.
* In the second code cell, make sure to enter the correct path to your data. For example:\
`listOfRootFilePaths = [path+"/data/v21/merged.root",]` 
  * In the notebook, you can run each cell of code/text one by one by using `shift`+`enter`. This will give you a chance to read the text and understand the code. To run everything from top to bottom in one go, press :fast_forward: in the top toolbar to restart and run the whole notebook
* All the plots will end uo in the created directory `/responseAndResolutionStudies/output`

# Generating histograms from SelectAndHistogram (AURORA):
* Move into the TLASteeringFullRun2 directory, if you don't have one, follow the instructions in the first section of this readme\
`cd TLASteeringFullRun2`
* Move into the SelectAndHistogram run directory\
`cd run/selectAndHistogram_onlineOverOffline`
* Make a directory to house the plots\
`mkdir plots`
  * Note that this directory will be shared by all the runs, and some plots may be overwritten if oyu are not careful when plotting later
* Change the version name of the output files to something arbitrary like v22, save and exit vim\
`vim run_selectAndHistogram_dataMjjOnlineOverOffline.sh`
* Submit using\
`sbatch run_selectAndHistogram_dataMjjOnlineOverOffline.sh`
* Open the run file, comment out the current "runNumber" and "tid", uncomment the ones below, save and exit\
`vim run_selectAndHistogram_dataMjjOnlineOverOffline.sh`
* submit again\
`sbatch run_selectAndHistogram_dataMjjOnlineOverOffline.sh`
* Repeat until you have submitted for all 13 datasetNumbers. I know this is a dumb way to do it, but due to the way sbatch works, this is the only way possible.
* When the jobs are finished the results should all appear inside an automatically created directory at `/TLASteeringFullRun2/run/onlineOverOffline/VERSIONNUMBER`

## General information
* The config lives at `TLASteeringFullRun2/configs/selectAndHistogram_onlineOverOffline/config_selectAndHistogram_onlineOverOffline.py`
  * Here you can change what calibration, triggers, and cuts are used. Currently, the config has defined that we only run HLT_175 jets with an eta cut of 2.4
* The run file lives at `TLASteeringFullRun2/run/selectAndHistogram_onlineOverOffline/run_selectAndHistogram_dataMjjOnlineOverOffline.sh`
* The algo lives at `TLASteeringFullRun2/src/TLAAlgosFullRun2/Root/SelectAndHistogram.cxx`
* The algo header file lives at `/TLASteeringFullRun2src/TLAAlgosFullRun2/TLAAlgosFullRun2/SelectAndHistogram.h`

# Plotting comparison plots form SelectAndHistogram (AURORA):
* Go out of TLASteeringFullRun2, and into the parent directory
`cd onlineOverOfflineStudies`
* Clone the dijet-TLA repository\
`git clone https://gitlab.cern.ch/atlas-phys-exotics-dijet-tla/dijet-TLA.git`
* Enter the new directory\
`cd dijet-TLA`
* Open the `setup_environment.sh` file, and create a user profile similar to that of `pekman`. Save and exit\
`vim setup_environment.sh`
* Run the setup\
`source setup_environment.sh`
* Enter the plotting directory\
`cd plotting`
* Edit the config\
`vim configs/config_selectAndHistogram_onlineOverOffline.py`
* Include the correct VERSIONNUMBER, as "tags".\
Line 40: `c.tags = ['VERSIONNUMBER'];`\
Line 41: `c.runsPerTag = {'VERSIONNUMBER': 'All'}`\
Line 47: `{'tags': ['VERSIONNUMBER'],`\
Line 48: `{'tags': ['VERSIONNUMBER'],`
* Run the python script\
`python quick_plot_run2.py ./configs/config_selectAndHistogram_onlineOverOffline.py`
